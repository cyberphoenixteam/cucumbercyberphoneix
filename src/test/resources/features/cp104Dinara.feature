@smoke
Feature:Creating Sales Channel in Configuration
  @cp152
  Scenario: Validating creating a new Sales Channel
    Given User login to the BriteERP and goes to the CRM module
    Then User goes to Sales Channels under Configuration
    And User clicks on Create button
    Then User filles all required fields