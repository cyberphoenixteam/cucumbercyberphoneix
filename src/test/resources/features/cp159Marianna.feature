@smoke
  Feature:CP-105 As a manager i should be able to Add new follower to members to members in the "Sales Channel" field

    @cp159
    Scenario:  Add new Follower field verification
      Given UserManager is logged to BriteBRP account and on Sales_Channel Page
      And UserManager Navigate to any Sales channel in a below list
      And UserManager Navigate and click on a person icon
      And UserManager On the Recipients field  and click's on Create and Edit new Recipient
      And UserManager files all the required fields
      Then UserManager saves all the information