package CRMModuleBRp.pages.steps;


import CRMModuleBRp.pages.pages.CP_107Page;
import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.pages.QuotationsPage;
import CRMModuleBRp.pages.utilities.Driver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class CP144_steps{
   QuotationsPage page = new QuotationsPage();
    LoginPage login = new LoginPage();
    HomePage homePage = new HomePage();



    @Given("Manager login to the BriteERP")
    public void manager_login_to_the_BriteERP() {
        login.loginToTheWebAppAsManager();
    }

    @Given("goes to the CRM module")
    public void goes_to_the_CRM_module() {
        homePage.goToCRMmodule();
    }

    @Given("Manager goes to Quatation module")
    public void manager_goes_to_Quatation_module() {
        page.quotationButton.click();
    }

    @Given("Manager clicks to the Import button")
    public void manager_clicks_to_the_Import_button()  {
        page.importButton.click();
    }


    @Then("Manager should see that expected URL is not matching with Actual ur")
    public void manager_should_see_that_expected_URL_is_not_matching_with_Actual_ur() {
        Assert.assertFalse("FAILED, expected URL is not matching with Actual url", Driver.getDriver().getCurrentUrl().contains("import"));
    }

}


