package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.CP_107Page;
import CRMModuleBRp.pages.pages.CP_128Page;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.utilities.Action;
import CRMModuleBRp.pages.utilities.Driver;
import CRMModuleBRp.pages.utilities.SelectDropdown;
import com.github.javafaker.Faker;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CP128_steps {

    CP_128Page page = new CP_128Page();


    @When("Manager should be able to open the web page login with valid credentials")
    public void manager_should_be_able_to_login_with_valid_credentials() {
        LoginPage crmLP = new LoginPage();
        crmLP.loginToTheWebAppAsManager();
    }

    @When("Manager should be able to click CRM Module")
    public void manager_should_be_able_to_click_CRM_Module() {
        WebElement CRMModuleButton = Driver.getDriver().findElement(By.xpath("//a[@href='/web#menu_id=261&action=365']"));
        CRMModuleButton.click();
    }

    @When("Manager should be able  to click Activity types")
    public void manager_should_be_able_to_click_Activity_types() throws InterruptedException {
        Thread.sleep(2000);
        page.activityTypeButton.click();
    }

    @Then("Manager should be able to create an activity by giving the correct information")
    public void manager_should_be_able_to_create_an_activity_by_giving_the_correct_information() {
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 5);
        wait.until(ExpectedConditions.elementToBeClickable(page.createButton));
        page.createButton.click();
        Faker faker = new Faker();
        String nameSendKeys = faker.name().name();
        page.inputName.sendKeys(nameSendKeys);
        String optionWillbeSelected = "Meeting";
        SelectDropdown.getSelect(page.categorySelect).selectByVisibleText(optionWillbeSelected);
        String summarySendKeys = faker.app().author();
        page.summaryBox.sendKeys(summarySendKeys);
        String numberOfDaysSendKeys = faker.number().digit();
        page.numberOfDays.sendKeys(numberOfDaysSendKeys);
        page.saveButton.click();

        String verifyName = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget o_required_modifier']")).getText();
        String verifyCategory = Driver.getDriver().findElement(By.xpath("//span[.='Meeting']")).getText();
        String verifySummary = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget']")).getText();
        String verifyNumberOfDays = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_integer o_field_number o_field_widget']")).getText();
        Assert.assertEquals("Name of activity type failed", nameSendKeys, verifyName);
        Assert.assertEquals("Category of activity type failed", optionWillbeSelected, verifyCategory);
        Assert.assertEquals("Summary of activity type failed", summarySendKeys, verifySummary);
        Assert.assertEquals("Number of days of activity type failed", numberOfDaysSendKeys, verifyNumberOfDays);
    }

    @Then("Manager should be able to see actions button and delete the newly created activity")
    public void manager_should_be_able_to_see_actions_button_and_delete_the_newly_created_activity() throws InterruptedException {
        String verifyNameBeforeDeletion = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget o_required_modifier']")).getText();
        page.actionButton.click();
        page.deleteButton.click();
        page.okButton.click();
        Action.getActions().moveToElement(page.activiTypeButton).click().perform();
        List<WebElement> allNames = Driver.getDriver().findElements(By.xpath("//tbody/tr/td[3]"));

        for (WebElement w : allNames) {

            Assert.assertTrue("Deletion of activity type failed", !verifyNameBeforeDeletion.equals(w.getText()));

        }
    }
}
