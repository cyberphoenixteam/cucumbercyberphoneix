package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.pages.PipelinePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class CP108_steps {

    PipelinePage pipelinePage = new PipelinePage();

    @Given("UserClient is logged to BriteBRP account and on Sales_Channel Page")
    public void userclient_is_logged_to_BriteBRP_account_and_on_Sales_Channel_Page() {

        LoginPage briteLogin = new LoginPage();
        briteLogin.loginToTheWebAppAsUser();
        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();
    }

    @When("UserClient is clicking on Calendar Button")
    public void userclient_is_clicking_on_Calendar_Button() {


       Assert.assertTrue("Calendar Button is not displayed",pipelinePage.calendarButton.isDisplayed());
        pipelinePage.calendarButton.click();
    }

    @Then("Then UserClient is present on Calendar")
    public void then_UserClient_is_present_on_Calendar() {
       Assert.assertTrue("Calendar button is not clickable",pipelinePage.calendarTodayButton.isDisplayed());
    }
}
