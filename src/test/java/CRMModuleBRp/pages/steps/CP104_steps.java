package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.ConfigurationSalesChannel;
import CRMModuleBRp.pages.utilities.Driver;
import com.github.javafaker.Faker;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CP104_steps {

    ConfigurationSalesChannel configurationSalesChannel = new ConfigurationSalesChannel();

    @Then("User goes to Sales Channels under Configuration")
    public void user_goes_to_Sales_Channels_under_Configuration() throws InterruptedException{
        Thread.sleep(3000);
        WebElement salesChannel = Driver.getDriver().findElement(By.xpath("//a[@data-menu='267']"));
        Thread.sleep(3000);
        salesChannel.click();
    }

    @Then("User clicks on Create button")
    public void user_clicks_on_Create_button() throws InterruptedException{
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(),10);
        Thread.sleep(3000);
        try {
            configurationSalesChannel.createButton.click();
        }catch (Exception e){
            wait.until(ExpectedConditions.elementToBeClickable(configurationSalesChannel.createButton));
            configurationSalesChannel.createButton.click();
        }


    }

   @Then("User filles all required fields")
    public void user_filles_all_required_fields() throws InterruptedException{
       Faker faker = new Faker();
       configurationSalesChannel.channelLeaderInputbox.sendKeys("Administrator"+Keys.ENTER);
       configurationSalesChannel.emailInputbox.sendKeys(faker.internet().emailAddress());
       configurationSalesChannel.addButton.click();

       Assert.assertTrue("Verification of the default value of Sales Channels name input box FAILED!",configurationSalesChannel.salesteamNameInputBox.isDisplayed());
       Assert.assertTrue("The label Quotaions is not Displayed by default. Verification FAILED!",configurationSalesChannel.quotations.isDisplayed());
       Assert.assertTrue("The label Pipeline is not Displayed",configurationSalesChannel.pipelineLabel.isDisplayed());

       configurationSalesChannel.channelsMemberAltynaiTurgaeva.click();
       configurationSalesChannel.createButton2.click();
       Thread.sleep(3000);
       configurationSalesChannel.saveAndClose.click();

       Assert.assertFalse("Verification of creating the Sales Channels FAILED!Error message is displayed",configurationSalesChannel.errorMessage.isDisplayed());

   }
}
