package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.pages.PipelinePage;
import CRMModuleBRp.pages.utilities.Driver;
import CRMModuleBRp.pages.utilities.SoftAssert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class CP100_steps {

    PipelinePage pipelinePage = new PipelinePage();

    @Given("User login to the BriteERP and goes to the CRM module")
    public void user_login_to_the_BriteERP_and_goes_to_the_CRM_module() {
        LoginPage loginPage = new LoginPage();
        loginPage.loginToTheWebAppAsManager();

        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();
    }

    @Then("User goes to Pipeline functionality")
    public void user_goes_to_Pipeline_functionality()throws InterruptedException {

    }

    @Then("User clicks to the Import button")
    public void user_clicks_to_the_Import_button() {
        pipelinePage.importButton.click();
    }


    @Then("User import window opens User clicks to LoadFile button and see the file")
    public void user_import_window_opens_User_clicks_to_LoadFile_button_and_see_the_file()throws InterruptedException {
        WebElement fileInputBox = Driver.getDriver().findElement(By.xpath("//input[@class='oe_import_file_show form-control']"));
        fileInputBox.sendKeys("SampleData.xlsx" + Keys.ENTER);
        String attributeValue = pipelinePage.fileInputBox.getAttribute("placeholder");
        Assert.assertTrue("Verification of importing excell file FAILED!",attributeValue.contains("No file chosen"));
    }


}