package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.pages.PipelinePage;


import com.github.javafaker.Faker;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;


public class CP147_steps {
    PipelinePage pp = new PipelinePage ();
    Faker faker = new Faker();

    @Given("Manager is logged to BriteBRP account and on CRM page")
    public void manager_is_logged_to_BriteBRP_account_and_on_CRM_page() {
        LoginPage login = new LoginPage();
        login.loginToTheWebAppAsManager();

        HomePage hPage = new HomePage();
        hPage.goToCRMmodule();


    }

    @Given("Manager navigate to Pipeline in a below list")
    public void manager_navigate_to_Pipeline_in_a_below_list() {
        pp.pipeline2.click();

    }

    @Given("Manager navigate and click on create")
    public void manager_navigate_and_click_on_create() {
    pp.create.click();


    }

    @Given("Manager on create opportunity field")
    public void manager_on_create_opportunity_field() {
    String actualText = pp.textMatches.getText();
        String expectedText1="Opportunity Title";
        Assert.assertEquals("Actual text doesnt match to expected Opportunity Title",actualText,expectedText1);

    }

    @Given("Manager files all the required fields")
    public void manager_files_all_the_required_fields() {
    pp.faker.sendKeys((faker.name().name()));
    pp.exRevenue.sendKeys("452");
    pp.ddowns.click();
    pp.dd.click();

    }

    @Then("Manager creates opportunity")
    public void manager_creates_opportunity() throws InterruptedException{
        Thread.sleep(1000);
    pp.createButton.click();

    }
}

