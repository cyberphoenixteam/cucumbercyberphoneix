package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.utilities.Driver;
import CRMModuleBRp.pages.utilities.SoftAssert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CP106_steps {

    WebDriver driver = Driver.getDriver();


    @Given("User: login to the BriteERP and goes to the CRM module")
    public void user_login_to_the_BriteERP_and_goes_to_the_CRM_module() throws InterruptedException {

        LoginPage briteLogin = new LoginPage();
        briteLogin.loginToTheWebAppAsUser();
        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();

        WebElement element1 = driver.findElement(By.xpath("//a[@href='/web#menu_id=261&action=365']"));
        element1.click();
        Assert.assertTrue("Element1 is not displayed", element1.isDisplayed());
        Thread.sleep(3000);

    }

    @Then("User: goes to Pipeline Page and clicks on Create button")
    public void user_goes_to_Pipeline_Page_and_clicks_on_Create_button() throws InterruptedException {
        WebElement element2 = driver.findElement(By.cssSelector("button.btn.btn-primary.btn-sm.o-kanban-button-new"));
        element2. click();
        Assert.assertTrue("Element2 is not displayed", element2.isDisplayed());
        Thread.sleep(2000);



    }

    @Then("User: filles all required fields")
    public void user_filles_all_required_fields() throws InterruptedException {


        WebElement element3 = driver.findElement(By.cssSelector("input.o_field_char.o_field_widget.o_input.o_required_modifier"));
        element3.sendKeys("Testing 123");
        Assert.assertTrue("Element2 is not displayed", element3.isDisplayed());
        Thread.sleep(1000);


        Actions myAction = new Actions(driver);

        WebElement button = driver.findElement(By.xpath("//div[@class='o_input_dropdown'][1]"));
        Assert.assertTrue("Button is not displayed", button.isDisplayed());
        Thread.sleep(1000);

        myAction.moveToElement(button).click().sendKeys("James Bond").build().perform();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//button[@class = 'btn btn-sm btn-primary'][2]")).click();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[text() = 'Create and edit']")).click();
        Thread.sleep(2000);


        WebElement one = driver.findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_address_street']"));
        one.sendKeys("555 Address one Str Unit 5");
        Thread.sleep(2000);

        driver.findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_address_street'][2]")).sendKeys("123 Address str unit 555");
        Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_address_city']")).sendKeys("Chicago");
        Thread.sleep(1000);

//      driver.findElement(By.id("o_field_input_221")).sendKeys("Illinois");
//        Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_address_zip']")).sendKeys("60055");
        Thread.sleep(1000);

//      driver.findElement(By.id("o_field_input_223")).sendKeys("USA");
//---------------------------

//    Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input']")).sendKeys("NUMBER1234567");
        Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@placeholder='e.g. Sales Director']")).sendKeys("Sales Director");
        Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@placeholder='e.g. www.odoo.com']")).sendKeys("Www.odoo.com");
        Thread.sleep(1000);

        WebElement PhoneField1 = driver.findElement(By.xpath("//input[@class='o_field_phone o_field_widget o_input']"));
        PhoneField1.sendKeys("555-555-5555-ABC");
        Assert.assertTrue("PhoneField1 contains characters: BUG", PhoneField1.getText().contains("ABC"));
//        driver.findElement(By.xpath("//input[@name='mobile']")).sendKeys("555-555-5555-ABC");;

        Thread.sleep(1000);

        WebElement email = driver.findElement(By.xpath("//input[@class='o_field_email o_field_widget o_input']"));
        email.sendKeys("Random.email");
        Assert.assertTrue("@ is not present: Bug", email.getText().contains("@"));
        Thread.sleep(1000);

        WebElement PhoneField = driver.findElement(By.xpath("(//input[@class='o_field_phone o_field_widget o_input'])[2]"));
        PhoneField.sendKeys("555-567-4567-ABC");
        Assert.assertTrue("PhoneField contains characters: BUG", PhoneField.getText().contains("ABC"));

        driver.findElement(By.xpath("(//input[@class='o_input ui-autocomplete-input'])[9]")).sendKeys("IT");

        SoftAssert.getSoftAssert().assertAll();


    }

}
