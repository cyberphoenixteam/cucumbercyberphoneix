package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.pages.HomePage;
import CRMModuleBRp.pages.pages.LoginPage;
import CRMModuleBRp.pages.pages.QuotationsPage;
import CRMModuleBRp.pages.utilities.SoftAssert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.Assert;

public class cp105_steps {
    QuotationsPage QP = new QuotationsPage();

    @When("User: login to ERP Brite WebApp")
    public void user_login_to_ERP_Brite_WebApp() {
        LoginPage briteLogin = new LoginPage();
        briteLogin.loginToTheWebAppAsUser();
        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();
    }

    @Then("User goes to Quotations Page")
    public void user_goes_to_Quotations_Page() throws InterruptedException {

        Assert.assertTrue("QuotationsButton is not Diplayed", QP.QuotationsButton.isDisplayed());
        Thread.sleep(2000);
        QP.QuotationsButton.click();
        Thread.sleep(2000);
        Assert.assertTrue("plusMinusButton is not Dsplayed", QP.plusMinusButton.isDisplayed());
        QP.plusMinusButton.click();
    }

    @Then("User clicks on Filter field")
    public void user_clicks_on_Filter_field() throws InterruptedException {
        Assert.assertTrue ("FiltersButton is not Dsplayed", QP.filterButton.isDisplayed());
        Thread.sleep(2000);
        QP.filterButton.click();
        Thread.sleep(2000);
    }



    @Then("User clicks on My order field")
    public void user_clicks_on_My_order_field() {
        Assert.assertTrue ("myOrdersButton is not Dsplayed", QP.myOrdersButton.isDisplayed());
        QP.myOrdersButton.click();
    }

}
