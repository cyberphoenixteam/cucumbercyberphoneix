package CRMModuleBRp.pages.steps;

import CRMModuleBRp.pages.utilities.Driver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {
     @After
    public void tearDown(Scenario scenario){
        System.out.println(scenario.getName());


        if(scenario.isFailed()){
            byte[] screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot,"image/png");
        }
        System.out.println(scenario.getStatus());
        Driver.closeDriver();
    }
}
