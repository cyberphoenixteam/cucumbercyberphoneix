package CRMModuleBRp.pages.pages;

import CRMModuleBRp.pages.utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuotationsPage {

    public WebDriver driver;

    public QuotationsPage(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver,this);

    }

    @FindBy(xpath = "(//span[contains (text(), 'Quotations')])[1]" )
    public WebElement quotationButton;

    @FindBy(xpath = "//button[@class='btn btn-sm btn-default o_button_import']")
        public WebElement importButton;

    @FindBy(xpath = "//input[@class='oe_import_file_show form-control']")
        public WebElement fileInputButton;



    @FindBy(linkText = ("Quotations"))
    public WebElement QuotationsButton;

    @FindBy (xpath = ("//span[@class='o_searchview_more fa fa-search-plus']"))
    public WebElement plusMinusButton;

    @FindBy (xpath = ("//span[@class='fa fa-filter']"))
    public WebElement filterButton;

    @FindBy (xpath = "//li[@data-index='0']")
    public WebElement myOrdersButton;
    }

