package CRMModuleBRp.pages.pages;

        import CRMModuleBRp.pages.utilities.Driver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.PageFactory;

public class PipelinePage {


    public PipelinePage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }


    @FindBy(xpath = "//span[contains(text(),'Pipeline')]")
    public WebElement pipeline2;

    @FindBy(xpath = "//button[contains(text(),'Import')]")
    public WebElement importButton;

    @FindBy(xpath = "//input[@class='oe_import_file_show form-control']")
    public WebElement fileInputBox;

    public void goToPipeline() {
        pipeline2.click();
    }


    @FindBy(xpath = "//a[@data-menu-xmlid='crm.menu_crm_opportunities']")
    public WebElement pipeline;

    @FindBy(xpath="//button[@class='btn btn-primary btn-sm o-kanban-button-new']")
    public WebElement create;

   @FindBy(xpath = "(//input[contains(@class,'autocomplete')])[1]")
    public WebElement ddowns;
   @FindBy(xpath = "//li[@class='ui-menu-item']")
   public  WebElement dd;

   @FindBy(xpath = "(//td[@class='o_td_label'])[1]")
    public WebElement textMatches;

   @FindBy(name="name")
    public WebElement faker;
   @FindBy(xpath = "//td//div[@class='o_priority o_field_widget']//a[@title='High']")
   public WebElement exRevenue;

   @FindBy(xpath = "//span[contains(text(),'Create')]")
    public WebElement createButton;

    @FindBy(xpath = "//button[@aria-label='calendar']")
    public WebElement calendarButton;

    @FindBy(xpath = "//button[@class='o_calendar_button_today btn btn-sm btn-primary']")
    public WebElement calendarTodayButton;

}
