package CRMModuleBRp.pages.pages;

import CRMModuleBRp.pages.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CP_128Page {

    public CP_128Page() { PageFactory.initElements(Driver.getDriver(), this); }

    @FindBy(linkText = "Activity Types")
    public WebElement activityTypeButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-sm o_list_button_add']")
    public WebElement createButton;

    @FindBy(xpath = "//input[@class='o_field_char o_field_widget o_input o_required_modifier']")
    public WebElement inputName;

    @FindBy(xpath = "//select[@class='o_input o_field_widget']")
    public WebElement categorySelect;

    @FindBy(xpath = "//input[@class='o_field_char o_field_widget o_input']")
    public WebElement summaryBox;

    @FindBy(xpath = "//input[@class='o_field_integer o_field_number o_field_widget o_input']")
    public WebElement numberOfDays;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-sm o_form_button_save']")
    public WebElement saveButton;

    @FindBy(xpath = "(//span[@class='caret'])[3]")
    public WebElement actionButton;

    @FindBy(xpath = "//a[@data-index='0']")
    public WebElement deleteButton;

    @FindBy(xpath = "//span[.='Ok']")
    public WebElement okButton;

    @FindBy(xpath = "//a[.='Activity Types']")
    public WebElement activiTypeButton;
}
