package CRMModuleBRp.pages.pages;

import CRMModuleBRp.pages.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfigurationSalesChannel {

    public ConfigurationSalesChannel (){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//div[@class='o_cp_left']//button")
    public WebElement createButton;

    @FindBy(xpath = "//input[@class='o_field_char o_field_widget o_input o_required_modifier']")
    public WebElement salesteamNameInputBox;

    @FindBy(xpath = "//label[.='Quotations']")
    public WebElement quotations;

    @FindBy(xpath = "//label[.='Pipeline']")
    public WebElement pipelineLabel;

    @FindBy(xpath = "//div[@name='user_id']//input")
    public WebElement channelLeaderInputbox;

    @FindBy(xpath = "//div[@name='edit_alias']//input")
    public WebElement emailInputbox;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-sm o-kanban-button-new']")
    public WebElement addButton;

    @FindBy(xpath = "//table[starts-with(@class,'o_list_view')]/tbody//tr[3]/td[1]")
    public WebElement channelsMemberAltynaiTurgaeva;

    @FindBy(xpath = "//button[@class='btn btn-sm btn-primary']")
    public WebElement createButton2;

    @FindBy(xpath = "//span[.='Save  & Close']")
    public WebElement saveAndClose;

    @FindBy(xpath = "//div[@class='o_notification undefined o_error']")
    public WebElement errorMessage;

}
