$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/cp144Altynai.feature");
formatter.feature({
  "name": "As a manager I should be able to check Import button in Quotation mark",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Checking url of import button",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@cp144"
    }
  ]
});
formatter.step({
  "name": "Manager login to the BriteERP",
  "keyword": "Given "
});
formatter.match({
  "location": "CP144_steps.manager_login_to_the_BriteERP()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "goes to the CRM module",
  "keyword": "And "
});
formatter.match({
  "location": "CP144_steps.goes_to_the_CRM_module()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Manager goes to Quatation module",
  "keyword": "And "
});
formatter.match({
  "location": "CP144_steps.manager_goes_to_Quatation_module()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Manager clicks to the Import button",
  "keyword": "And "
});
formatter.match({
  "location": "CP144_steps.manager_clicks_to_the_Import_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Manager should see that expected URL is not matching with Actual ur",
  "keyword": "Then "
});
formatter.match({
  "location": "CP144_steps.manager_should_see_that_expected_URL_is_not_matching_with_Actual_ur()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});