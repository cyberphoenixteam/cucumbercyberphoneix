@smoke
  Feature: CP 128 Create and delete an activity type
    @cp128
    Scenario: As a manager I should be able to create and delete an Activity Type.
      When Manager should be able to open the web page login with valid credentials
      And Manager should be able to click CRM Module
      And Manager should be able  to click Activity types
      Then Manager should be able to create an activity by giving the correct information
      Then Manager should be able to see actions button and delete the newly created activity