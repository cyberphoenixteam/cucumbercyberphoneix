@smoke
Feature:As a manager I should be able to create a Pipeline for the Customer
  @cp147
  Scenario: Create an Opportunity in Pipeline field
    Given Manager is logged to BriteBRP account and on CRM page
    And  Manager navigate to Pipeline in a below list
    And  Manager navigate and click on create
    And Manager on create opportunity field
    And Manager files all the required fields
    Then Manager creates opportunity