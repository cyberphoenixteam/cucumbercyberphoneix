
@smoke
Feature: Pipeline page: User page creation.
  @cp106
  Scenario: User should be able to create Pipeline Page
    Given User: login to the BriteERP and goes to the CRM module
    Then User: goes to Pipeline Page and clicks on Create button
    And User: filles all required fields
